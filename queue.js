let collection = [];

// Write the queue functions below.

function print(){
	if (collection.length > 0) {
		for (let i=0; i < collection.length; i++){
			console.log(collection[i])
		}
	}
	else {
		return collection
	}
}

function enqueue(newItem) {
	let i = collection.length

	collection[i]=newItem
	return collection
}


function dequeue() {
	let newArray = []

	for(let i=1; i < collection.length ;i++){
		newArray[i-1]=collection[i]
	}

	collection=newArray

	return collection
}


function front() {
	return collection[0]
}


function size() {
	return collection.length
}


function isEmpty() {
	if (collection.length > 0){
		return false
	}
	else {
		return true
	}
}



module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};